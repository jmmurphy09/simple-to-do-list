<?php
include('http_response_code.php');
$method = $_SERVER['REQUEST_METHOD'];
try {
	$db = new PDO('mysql:host=localhost;dbname=todo;charset=utf8', 'todo', 'todo');
} catch (PDOException $e) {
	die($e->getMessage());
}

if (isset($_REQUEST['task_id']) && is_numeric($_REQUEST['task_id'])) {
	$task_id = $_REQUEST['task_id'];
}

switch ($method)
{
	case 'GET':
		doGet();
		break;
	case 'POST':
		doPost();
		break;
	case 'PUT':
		doPut();
		break;
	case 'DELETE':
		doDelete();
		break;
	default:
		die();
}

function doGet()
{
	global $db;
	$stmt = $db->query("SELECT * FROM tasks");
	$result = $stmt->fetchAll();
	$tasks = array();
	foreach($result as $t) {
		$tasks[] = array('task_id' => $t['task_id'], 'text' => $t['text'], 'complete' => $t['complete']);
	}
	echo json_encode($tasks);
}

function doPut()
{
	global $db, $task_id;
	if (!isset($task_id)) {
		http_response_code(400);
		return;
	}
	$stmt = $db->prepare("UPDATE tasks SET text=:text, complete=:complete WHERE task_id=:task_id");
	$json = json_decode(file_get_contents("php://input"));
	$stmt->bindParam(':text',$json->text);
	$stmt->bindParam(':complete',$json->complete);
	$stmt->bindParam(':task_id',$json->task_id);
	$stmt->execute();
}

function doPost()
{
	global $db;
	$json = json_decode(file_get_contents("php://input"));
	if (!isset($json->text)) {
		http_response_code(400);
		return;
	}
	$stmt = $db->prepare("INSERT INTO tasks (text, complete) VALUES (:text, 0)");
	$stmt->bindParam(':text', $json->text);
	if ($stmt->execute()) {
		http_response_code(201);
		$response = array('task_id' => $db->lastInsertId());
		echo json_encode($response);
	} else {
		http_response_code(500);
	}
}

function doDelete()
{
	global $db, $task_id;
	if (!isset($task_id)) {
		http_response_code(400);
		return;
	}
	$stmt = $db->prepare("DELETE FROM tasks WHERE task_id=:task_id");
	$stmt->bindParam(':task_id', $task_id);
	if ($stmt->execute()) {
		http_response_code(204);
	} else {
		http_response_code(500);
	}
}

?>