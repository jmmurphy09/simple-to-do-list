$(document).ready(function() {
	$.ajax({
		url: "tasks.php",
		type: "GET",
		dataType: "json",
		success: function(result) {
			$('#loading').hide();
			$('#tasks').show();
			for (var i = 0; i < result.length; ++i)
			{
				var task = result[i];
				var html = '<tr><td><input class="complete" type="checkbox" ';
				if (task.complete == 1) {
					html += 'checked';
				}
				html += '/>';
				html += '</td><td';
				if (task.complete == 1) {
					html += ' class="complete"';
				}
				html += '>' + task.text + '</td>';
				html += '<td><a href="#" class="edit">Edit</a></td>';
				html += '<td><a href="#" class="delete">Delete</a></td>';
				html += '</tr>';
				$('#createNew').before(html);
				$('#createNew').prev().data('record_id', task.task_id);
			}
		}
	});
	
	$('body').on('click', 'a.edit', function(event) {
		var taskTextCell = $(event.target).parent().prev();
		var taskText = taskTextCell.text();
		taskTextCell.html('<input type="text" value="' + taskText + '" />');
		$(event.target).removeClass('edit');
		$(event.target).addClass('save');
		$(event.target).text('Save');
	});
	
	$('body').on('click', 'a.save', function(event) {
		var taskTextCell = $(event.target).parent().prev();
		var taskRow = taskTextCell.parent();
		var complete = taskRow.find('input.complete').is(':checked');
		var task = {};
		task.task_id = taskRow.data('record_id');
		task.text = taskTextCell.find('input').val();
		task.complete = complete;
		$.ajax({
			url: "tasks.php?task_id="+task.task_id,
			type: "PUT",
			data: JSON.stringify(task),
			contentType: "application/json",
			success: function(result) {
				$(event.target).removeClass('save');
				$(event.target).addClass('edit');
				$(event.target).text('Edit');
				taskTextCell.html(task.text);
			}
		});
	});
	
	$('body').on('click', 'a.delete', function(event) {
		var taskTextCell = $(event.target).parent().prev();
		var taskRow = taskTextCell.parent();
		var task = {};
		task.task_id = taskRow.data('record_id');
		$.ajax({
			url: "tasks.php?task_id="+task.task_id,
			type: "DELETE",
			data: JSON.stringify(task),
			contentType: "application/json",
			success: function(result) {
				taskRow.remove();
			}
		});
	});
	
	$('body').on('click', '#save', function(event) {
		var saveCell = $(event.target).parent();
		var completeCell = saveCell.prev().prev();
		var deleteCell = saveCell.next();
		var taskTextCell = saveCell.prev();
		var taskRow = saveCell.parent();
		var task = {};
		task.text = taskTextCell.find('input').val();
		$.ajax({
			url: "tasks.php",
			type: "POST",
			data: JSON.stringify(task),
			contentType: "application/json",
			success: function(result) {
				var html = '<tr><td><input class="complete" type="checkbox" /></td><td>' + task.text + '</td><td><a href="#" class="save">Edit</a></td><td><a href="#" class="delete">Delete</a></td></tr>';
				$('#createNew').before(html);
				$('#createNew').prev().data('record_id', result.task_id);
				taskTextCell.find('input').val('');
			}
		});
	});
	
	$('body').on('change', 'input[type="checkbox"]', function(event) {
		var taskTextCell = $(event.target).parent().next();
		var taskRow = taskTextCell.parent();
		var complete = $(event.target).is(':checked');
		var task = {};
		task.task_id = taskRow.data('record_id');
		if (taskTextCell.find('input').val() != undefined) {
			task.text = taskTextCell.find('input').val();
		} else {
			task.text = taskTextCell.text();
		}
		task.complete = complete;
		$.ajax({
			url: "tasks.php?task_id="+task.task_id,
			type: "PUT",
			data: JSON.stringify(task),
			contentType: "application/json",
			success: function(result) {
				if (complete) {
					taskTextCell.addClass('complete');
				} else {
					taskTextCell.removeClass('complete');
				}
			}
		});
	});
});